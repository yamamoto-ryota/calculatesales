package jp.alhinc.yamamoto_ryota.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ
		if(args.length != 1) {
        	System.out.println("予期せぬエラーが発生しました。");
        	return;
        }

        System.out.println("ここにあるファイルを開きます=>" + args[0]);

        
        //マップの生成
        Map<String, String> branchNames = new HashMap<>();
        Map<String, Long> branchSales = new HashMap<>();
        //支店定義ファイル読み込み処理
        BufferedReader br1 = null;
        try {
        	File file = new File(args[0], "branch.lst");
        	FileReader fr1 = new FileReader(file);
        	br1 = new BufferedReader(fr1);

        	String line;
        	while((line = br1.readLine()) != null) {
        		String[] items = line.split(",");
        		//支店定義ファイルフォーマットチェック
        		if((items.length != 2) || (!items[0].matches("^[0-9]{3}$"))) {
        			System.out.println("支店定義ファイルのフォーマットが不正です。");
        			return;
        		}

        		branchNames.put(items[0], items[1]);
        		branchSales.put(items[0],0L);
        	}
        } catch(IOException e) {
        	System.out.println("予期せぬエラーが発生しました。");
        }finally {
        	if(br1 != null) {
        		try {
        			br1.close();
        		}catch(IOException e) {
        			System.out.println("予期せぬエラーが発生しました。");
        		}
        	}
        }

        //リストへの追加
        File[] files = new File(args[0]).listFiles();
        List<File> rcdFiles = new ArrayList<>();
        for(int i=0; i<files.length; i++ ) {
        	String fileName = files[i].getName();
        	if(fileName.matches("^[0-9]{8}.rcd$")){
        		rcdFiles.add(files[i]);
        	}
        }

        //連番チェック
        for(int i=0; i < rcdFiles.size() - 1; i++) {
        	int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
        	int latter = Integer.parseInt(rcdFiles.get(i+1).getName().substring(0,8));

        	if((latter - former) != 1) {
        		System.out.println("売り上げファイル名が連番になっていません。");
        	}
        }


        //支店コード,売上額集計

        for(int i=0; i<rcdFiles.size(); i++ ) {
        	
        	try {
            	br1 = new BufferedReader(new FileReader(rcdFiles.get(i)));
            	//該当ファイル名
            	String fileName = rcdFiles.get(i).getName();
            	
            	ArrayList<String> salesList = new ArrayList<>() ;
            	String line = "";
            	while((line = br1.readLine()) != null) {
            	salesList.add(line);
            	}
            	//売り上げファイルが2行でなかった場合、エラーメッセージを表示
            	if(salesList.size() != 2) {
            	System.out.println("<" + fileName + ">のフォーマットが不正です");
            	return;
            	}

            	//2行目が数字かどうか
            	String money = (String)salesList.get(1);
            	if(!money.matches("^[0-9]+$")) {
            		System.out.println("予期せぬエラーが発生しました。");
            		return;
            	}
            	//支店コード
            	String branchCode = (String) salesList.get(0);
            	
            	//売り上げファイルの支店コード=支店定義ファイルの支店コードかどうかチェック
            	if(!branchNames.containsKey(branchCode)) {
            	 System.out.println("<" + fileName + ">の支店コードが不正です。");
            	 return;
            	}

            	//売上金額と、branchSalesマップへの加算
            	long salesValue = Long.parseLong(money);
            	Long salesAmount = branchSales.get(branchCode) + salesValue;
            	
            	//合計金額が11桁以上であるかチェック
            	if(salesAmount >= 10000000000L) {
            		System.out.println("合計金額が10桁を超えました。");
            		return;
            	    }
            	
            	branchSales.put(branchCode, salesAmount);

            } catch(IOException e) {
            	System.out.println("予期せぬエラーが発生しました。");
            }finally {
            	if(br1 != null) {
            		try {
            			br1.close();
            		}catch(IOException e) {
            			System.out.println("予期せぬエラーが発生しました。");
            		}
            	}
            }
        }
        System.out.println(branchNames);
        System.out.println(branchSales);

        //支店売り上げファイルへの出力
        try {
        	File outputFile =new File(args[0],"branch.out");
            FileWriter fw = new FileWriter(outputFile);
            BufferedWriter bw = new BufferedWriter(fw);

            bw.write("【支店別集計ファイル】");
            bw.newLine();

            for(String key : branchNames.keySet()) {
            	bw.write(key + ","+branchNames.get(key) + "," + branchSales.get(key));
            	bw.newLine();
            }

            bw.close();
        } catch (IOException e) {
        	System.out.println("予期せぬエラーが発生しました。");
        	return;
        }
        
        boolean checkInput = inputFiles(args[0], "branch.lst", branchNames, branchSales);
        if(!checkInput) {
        	return;
        }
        
        boolean checkAggregation = aggregationProcess(args[0], branchNames, branchSales);
        if(!checkAggregation) {
        	return;
        }
        
        boolean checkOutput = outputFiles(args[0], "branch.out", branchNames, branchSales);
        if(!checkOutput) {
        	return;
        }

	}
	
	

	private static boolean inputFiles(String filePath,
			String fileName,
			Map<String, String> branchNamesMap,
			Map<String, Long> branchSalesMap) {
		 
		//支店定義ファイル読み込み処理
        BufferedReader br1 = null;
        try {
        	File file = new File(filePath, fileName);
        	FileReader fr1 = new FileReader(file);
        	br1 = new BufferedReader(fr1);

        	String line;
        	while((line = br1.readLine()) != null) {
        		String[] items = line.split(",");
        		//支店定義ファイルフォーマットチェック
        		if((items.length != 2) || (!items[0].matches("^[0-9]{3}$"))) {
        			System.out.println("支店定義ファイルのフォーマットが不正です。");
        			return false;
        		}

        		branchNamesMap.put(items[0], items[1]);
        		branchSalesMap.put(items[0],0L);
        	}
        } catch(IOException e) {
        	System.out.println("予期せぬエラーが発生しました。");
        	return false;
        }finally {
        	if(br1 != null) {
        		try {
        			br1.close();
        		}catch(IOException e) {
        			System.out.println("予期せぬエラーが発生しました。");
        			return false;
        		}
        	}
        }

		return true;
	}
	
	
	private static boolean aggregationProcess(String filePath,
			Map<String, String> branchNamesMap,
			Map<String, Long> branchSalesMap) {
		 //リストへの追加
        File[] files = new File(filePath).listFiles();
        List<File> rcdFiles = new ArrayList<>();
        for(int i=0; i<files.length; i++ ) {
        	String filesName = files[i].getName();
        	if(filesName.matches("^[0-9]{8}.rcd$")){
        		rcdFiles.add(files[i]);
        	}
        }

        //連番チェック
        for(int i=0; i < rcdFiles.size() - 1; i++) {
        	int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
        	int latter = Integer.parseInt(rcdFiles.get(i+1).getName().substring(0,8));

        	if((latter - former) != 1) {
        		System.out.println("売り上げファイル名が連番になっていません。");
        		return false;
        	}
        }
        
      //支店コード,売上額集計

        for(int i=0; i<rcdFiles.size(); i++ ) {
            BufferedReader br1 = null;
        	try {
            	br1 = new BufferedReader(new FileReader(rcdFiles.get(i)));
            	//該当ファイル名
            	String fileName = rcdFiles.get(i).getName();
            	
            	ArrayList<String> salesList = new ArrayList<>() ;
            	String line = "";
            	while((line = br1.readLine()) != null) {
            	salesList.add(line);
            	}
            	//売り上げファイルが2行でなかった場合、エラーメッセージを表示
            	if(salesList.size() != 2) {
            	System.out.println("<" + fileName + ">のフォーマットが不正です");
            	return false;
            	}

            	//2行目が数字かどうか
            	String money = (String)salesList.get(1);
            	if(!money.matches("^[0-9]+$")) {
            		System.out.println("予期せぬエラーが発生しました。");
            		return false;
            	}
            	//支店コード
            	String branchCode = (String) salesList.get(0);
            	
            	//売り上げファイルの支店コード=支店定義ファイルの支店コードかどうかチェック
            	if(!branchNamesMap.containsKey(branchCode)) {
            	 System.out.println("<" + fileName + ">の支店コードが不正です。");
            	 return false;
            	}

            	//売上金額と、branchSalesマップへの加算
            	long salesValue = Long.parseLong(money);
            	Long salesAmount = branchSalesMap.get(branchCode) + salesValue;
            	
            	//合計金額が11桁以上であるかチェック
            	if(salesAmount >= 10000000000L) {
            		System.out.println("合計金額が10桁を超えました。");
            		return false;
            	    }
            	
            	branchSalesMap.put(branchCode, salesAmount);

            } catch(IOException e) {
            	System.out.println("予期せぬエラーが発生しました。");
            	return false;
            }finally {
            	if(br1 != null) {
            		try {
            			br1.close();
            		}catch(IOException e) {
            			System.out.println("予期せぬエラーが発生しました。");
            			return false;
            		}
            	}
            }
        }
		return true;
	}
  
	
	private static boolean outputFiles(String filePath,
			String fileName,
			Map<String, String> branchNamesMap,
			Map<String, Long> branchSalesMap) {
		  //支店売り上げファイルへの出力
        try {
        	File outputFile =new File(filePath,fileName);
            FileWriter fw = new FileWriter(outputFile);
            BufferedWriter bw = new BufferedWriter(fw);

            bw.write("【支店別集計ファイル】");
            bw.newLine();

            for(String key : branchNamesMap.keySet()) {
            	bw.write(key + ","+branchNamesMap.get(key) + "," + branchSalesMap.get(key));
            	bw.newLine();
            }

            bw.close();
        } catch (IOException e) {
        	System.out.println("予期せぬエラーが発生しました。");
        	return false;
        }
		return true;
	}
}
